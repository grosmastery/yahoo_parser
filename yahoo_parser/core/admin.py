from django.contrib import admin
from .models import *


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ("title", "news_type", "news_position", "news_url", "photo_link")


@admin.register(Keyword)
class KeywordAdmin(admin.ModelAdmin):
    list_display = ("title",)
