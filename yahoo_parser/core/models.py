from django.db import models


class Keyword(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Keywords"


class News(models.Model):
    title = models.CharField(max_length=255)
    photo_link = models.TextField()
    news_type = models.CharField(max_length=10)
    news_url = models.TextField()
    news_position = models.IntegerField(default=1)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "News"
