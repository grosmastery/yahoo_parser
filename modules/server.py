from datetime import datetime
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import ssl
import glob
import requests
from selenium import webdriver
from selenium.common import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from dotenv import load_dotenv
from load_django import *
from parser_app.models import *

load_dotenv()


class Post:

    def __init__(self):
        os.makedirs('screenshot', exist_ok=True)
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--window-size=1980, 1080")
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get("https://finance.yahoo.com/")
        self.driver.maximize_window()

    def parse_news(self):

        main = "main"
        right = "right"
        bottom = "bottom"
        screenshot_taken = False
        keywords = Keyword.objects.all()
        changed_news_type = []

        time.sleep(2)

        big_post_title = self.driver.find_element(
            By.XPATH, "//div[@class='Pos(r) dustyImage W($ntkLeadWidth) Fl(start) article-cluster-boundary'] //h2")
        big_post_img = self.driver.find_element(By.XPATH, "//div[@class='Pos(r) C($primaryColor) H(0) Ovy(h)'] //img")
        big_post_url = self.driver.find_element(
            By.XPATH, "//div[@class='Pos(r) dustyImage W($ntkLeadWidth) Fl(start) article-cluster-boundary'] //a")
        small_posts_title = self.driver.find_elements(By.XPATH, "//ul[@class='Pos(r) article-cluster-boundary'] //h3")
        small_posts_img = self.driver.find_elements(By.XPATH, "//ul[@class='Pos(r) article-cluster-boundary'] //img")
        small_posts_url = self.driver.find_elements(By.XPATH, "//ul[@class='Pos(r) article-cluster-boundary'] //a")

        news_type = main
        url = big_post_url.get_attribute('href')

        post, created = News.objects.get_or_create(title=big_post_title.text.strip(),
                                                   photo_link=big_post_img.get_attribute('src'),
                                                   news_url=url,
                                                   news_type=main,
                                                   news_position=1,
                                                   )

        if created:
            news = News.objects.filter(news_url=url).order_by('-id')[:2]
            if len(news) == 1 or news[0].news_position < news[1].news_position:
                changed_news_type.append([news_type, url, True])
                if not screenshot_taken:
                    self.take_screenshot()
                    screenshot_taken = True

        for title, img, url, idx in zip(small_posts_title, small_posts_img, small_posts_url, range(6)):
            news_type = right if idx < 3 else bottom
            title = title.text.strip()
            url = url.get_attribute('href')
            post, created = News.objects.get_or_create(title=title,
                                                       photo_link=img.get_attribute('src'),
                                                       news_type=news_type,
                                                       news_url=url,
                                                       news_position=idx+2)

            if created:
                news = News.objects.filter(news_url=url).order_by('-id')[:2]
                if len(news) == 1 or news[0].news_position < news[1].news_position:
                    boolean = any(key.title.lower() in title.lower() for key in keywords)
                    changed_news_type.append([news_type, url, boolean])
                    if not screenshot_taken:
                        self.take_screenshot()
                        screenshot_taken = True

        return changed_news_type

    def send_main(self, changed_news_type):
        sender_email = os.getenv("EMAIL_ADDRESS")
        receiver_email_main = os.getenv("EMAIL_ADDRESS_MAIN")
        receiver_email_other = os.getenv("EMAIL_ADDRESS_OTHER")
        password = os.getenv("EMAIL_PASS")

        for news_type, url, boolean in changed_news_type:
            receiver_email = receiver_email_main if boolean else receiver_email_other
            message = MIMEMultipart("alternative")
            message["Subject"] = news_type
            message["From"] = sender_email
            message["To"] = receiver_email
            text = f"""\
            {url}
            """

            message.attach(MIMEText(text, 'plain'))

            image_folder = "screenshot"
            image_files = glob.glob(os.path.join(image_folder, '*.png'))
            last_image_file = max(image_files, key=os.path.getctime)

            with open(last_image_file, 'rb') as file:
                mime = MIMEBase('image', 'png', filename='img.png')
                mime.add_header('Content-Disposition', 'attachment', filename='img1.png')
                mime.add_header('X-Attachment-Id', '0')
                mime.add_header('Content-ID', '<0>')
                mime.set_payload(file.read())
                encoders.encode_base64(mime)
                message.attach(mime)

            context = ssl.create_default_context()
            with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
                server.login(sender_email, password)
                server.sendmail(
                    sender_email, receiver_email, message.as_string()
                )

    def send_telegram_message(self, messages):
        bot_token = '6020789111:AAHc24yLgs7Z5OLKOqqCPuLrLn6T18WJ_Mg'
        chats_id = ['-1001892247853', '-1001919848463']
        for message, news_url, _ in messages:
            image_folder = "screenshot"
            image_files = glob.glob(os.path.join(image_folder, '*.png'))
            last_image_file = max(image_files, key=os.path.getctime)

            with open(last_image_file, 'rb') as file:
                files = {'photo': file}
                url = f'https://api.telegram.org/bot{bot_token}/sendPhoto'
                for chat_id in chats_id:
                    response = requests.post(url, data={'chat_id': chat_id, 'caption': f'{message}: {news_url}'}, files=files)

    def take_screenshot(self):
        t = str(datetime.now().strftime("%y-%m-%d_%H-%M-%S"))
        ele = self.driver.find_element(By.ID, "Col1-0-ThreeAmigos-Proxy")
        ele.screenshot(f'screenshot/created_at_{t}.png')

    def close_lightbox_container(self):
        try:
            WebDriverWait(self.driver, 10).until(
                ec.presence_of_element_located((By.XPATH, "//html[@id='atomic']"))).send_keys(Keys.ESCAPE)
        except StaleElementReferenceException:
            pass

    def main(self):
        self.close_lightbox_container()
        changed_news_type = self.parse_news()
        if changed_news_type:
            self.send_main(changed_news_type)
            self.send_telegram_message(changed_news_type)
            time.sleep(5)
            self.driver.refresh()


if __name__ == "__main__":
    Post().main()
