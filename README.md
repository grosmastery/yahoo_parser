# Yahoo Finance News Parser
This Python script is a web scraper designed to extract and monitor the latest news articles from Yahoo Finance. It uses Selenium to navigate the website, collect news articles from different sections, and notifies users via email and Telegram about updates based on specified keywords.

### Features:


- **Web Scraping:** Gathers news articles from the main section, right sidebar, and bottom section of Yahoo Finance
- **Database Integration:** Utilizes Django ORM to store news articles in a database.
- **Email Notifications:** Sends email alerts containing relevant news articles to specified recipients.
- **Telegram Notifications:** Notifies users about updates via Telegram, including attached screenshots of the articles.
- **Keyword Matching:** Filters news articles based on predefined keywords.
- **Screenshot Capturing:** Takes screenshots of the webpage when significant news updates are detected.
### Setup and Usage:
1. Install required Python packages listed in `requirements.txt`.
1. Set up a `.env` file with necessary credentials (email, Telegram bot token, etc.).
1. Ensure Chrome WebDriver is installed and referenced properly.
1. Run the script python `main.py` to start monitoring Yahoo Finance for news updates.


### Dependencies:
- Python 3.x
- Selenium
- Django
- dotenv
- Requests

### Usage:
Ensure the necessary environment variables are set and the dependencies are installed before executing the script. This script is designed for continuous monitoring, refreshing every 5 seconds to check for updates.
